import { ToastContainer } from 'react-toastify';
import './globals.css';
import type { AppProps } from 'next/app';
import 'react-toastify/dist/ReactToastify.css';
import { GoogleAnalytics } from '@next/third-parties/google';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Component {...pageProps} />
      <ToastContainer position="bottom-right" stacked limit={3} />
      <GoogleAnalytics gaId="G-041S8Y1ERX" />
    </>
  );
}
