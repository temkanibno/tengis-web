import { Footer } from '@/components/shared/footer';
import { NavbarPlaceholder } from '@/components/shared/navbar-plaholder';
import { NavigationBar } from '@/components/shared/navigation-bar';
import { OffMenu } from '@/components/shared/off-menu';
import { getMovies } from '@/services/getMovies';
import { getPage } from '@/services/getPage';
import { getMenusByKey } from '@/services/menu';
import { FullMovie } from '@/types';
import { Menu, StaticPage } from '@prisma/client';
import { GetServerSideProps, GetServerSidePropsContext, InferGetServerSidePropsType } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import { nanoid } from 'nanoid';
import styles from './page.module.css';
import { FavIcon } from '@/components/shared/fav';

type Repo = {
  mainMenus: Menu[];
  socialMenus: Menu[];
  footMenus: Menu[];
  movies: FullMovie[];
  page: StaticPage;
};

export default function Page({ repo }: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { mainMenus, socialMenus, movies, footMenus, page } = repo;
  const [menuOpen, setMenuOpen] = useState(false);

  return (
    <>
      <Head>
        <title>{page.title} .: Tengis.mn :.</title>
        <meta content={`${page.description}`} name="description" />
        <meta content={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${page.imagePath}_large.webp`} property="og:image" />
        <meta content={page.title} property="og:title" />
        <meta content={`${page.description}`} property="og:description" />
        <FavIcon />
      </Head>
      <OffMenu menus={mainMenus} open={menuOpen} onClose={() => setMenuOpen(false)} />
      <NavigationBar menus={mainMenus} handleMenuOpen={() => setMenuOpen(true)} />
      <NavbarPlaceholder />
      <div className="container my-10">
        <div className="grid grid-cols-12 gap-6">
          <div className="col-span-12 md:col-span-8">
            <div className="flex flex-col gap-6">
              {page.imagePath && <Image src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${page.imagePath}_large.webp`} width={1080} height={607.5} alt={page.title} />}
              <h1 className="text-6xl text-center text-white">{page.title}</h1>
              <div className={`${styles.content}`}>{<div dangerouslySetInnerHTML={{ __html: page.content || '' }} />}</div>
            </div>
          </div>
          <div className="col-span-12 md:col-span-4">
            <div className="grid grid-cols-2 gap-4">
              {movies.map((item) => (
                <Link href={`/film/${item.slug}`} key={nanoid()} title={item.title}>
                  <div className="aspect-[3/4] relative">
                    <Image
                      src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${item.verticalPosterUrl}`}
                      width={300}
                      height={400}
                      alt={item.title}
                      className="absolute top-0 left-0 object-cover w-full h-full rounded-md"
                    />
                  </div>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
      <Footer socialMenus={socialMenus} footMenus={footMenus} />
    </>
  );
}

export const getServerSideProps = (async (context: GetServerSidePropsContext) => {
  const { params } = context;
  const slug = params?.slug;

  const page = await getPage(slug as string);
  if (!page) {
    return {
      notFound: true,
    };
  }
  const movies = await getMovies({ type: 'ONGOING', orderType: 'asc' });
  const mainMenus = await getMenusByKey('main');
  const socialMenus = await getMenusByKey('social');
  const footMenus = await getMenusByKey('foot');

  const repo: Repo = { mainMenus, movies, page, socialMenus, footMenus };
  return { props: { repo } };
}) satisfies GetServerSideProps<{ repo: Repo }>;
