import { Footer } from '@/components/shared/footer';
import { NavigationBar } from '@/components/shared/navigation-bar';
import { getMovie } from '@/services/getMovie';
import { getMenusByKey } from '@/services/menu';
import { FullMovie, FullSession, MovieSessionArea, MovieSessionSeat, UserSession } from '@/types';
import { Menu, MovieSessionTicket } from '@prisma/client';
import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import { nanoid } from 'nanoid';
import { Chip } from '@/components/shared/chip';
import { useEffect, useState } from 'react';
import { OffMenu } from '@/components/shared/off-menu';
import { FavIcon } from '@/components/shared/fav';
import { getSession } from '@/services/getSession';
import dayjs from 'dayjs';
import { getSeats } from '@/services/getSeats';
import { SeatItems } from '@/components/seats/seats';
import { IconMail, IconPhone, IconPlus, IconTicket, IconUser } from '@tabler/icons-react';
import { currencyFormat } from '@/utils/currency-formatter';
import { Progress } from '@/components/shared/progress';
import { useOrder } from '@/hooks/useOrder';
import { useRouter } from 'next/router';
import { createUserSession } from '@/services/createUserSession';
import { Countdown } from '@/components/shared/countdown';
import { toast } from 'react-toastify';
import { useFormik } from 'formik';
import { TextField } from '@/components/shared/textfield';
import * as Yup from 'yup';

type Repo = {
  mainMenus: Menu[];
  socialMenus: Menu[];
  footMenus: Menu[];
  movie: FullMovie;
  session: FullSession;
  userSession: UserSession;
  seats: MovieSessionSeat[][];
};
type BookingTicket = {
  ticket: MovieSessionTicket;
  quantity: number;
};

export default function Page({ repo }: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { mainMenus, movie, socialMenus, session, seats, userSession, footMenus } = repo;
  const { orders, setOrders } = useOrder();
  const router = useRouter();
  const [selectedSeats, setSelectedSeats] = useState<MovieSessionSeat[]>([]);
  const [tickets, setTickets] = useState<BookingTicket[]>([]);
  const [menuOpen, setMenuOpen] = useState(false);

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: Yup.object().shape({
      phone: Yup.string()
        .required('Утасны дугаар оруулна уу')
        .matches(/^[1|6-9]{1,1}[0-9]{7}$/, 'Утасны дугаар буруу байна'),
      email: Yup.string().required('И-Мэйл оруулна уу').email('Имэйл хаяг буруу байна'),
      name: Yup.string()
        .required('Нэр оруулна уу')
        .matches(/^[a-zA-Zа-яА-ЯөүӨҮ\s-]*$/, 'Нэр буруу байна')
        .min(2, 'Нэрний урт хамгийн багадаа 2 тэмдэгт байх ёстой')
        .max(30, 'Нэрний урт хамгийн ихдээ 30 тэмдэгт байх ёстой'),
    }),
    initialValues: {
      name: '',
      email: '',
      phone: '',
    },
    onSubmit: (values) => {
      handleOrder();
    },
  });

  const totalTickets = tickets.reduce((acc, ticket) => acc + ticket.quantity, 0);
  const totalAmount = tickets.reduce((acc, ticket) => acc + ticket.quantity * Number(ticket.ticket.price), 0);
  const sessionTickets = session.tickets.filter((ticket) => ticket.title !== '');

  useEffect(() => {
    setOrders({});
    const bookingTickets = sessionTickets.map((ticket) => ({
      ticket,
      quantity: 0,
    }));
    setTickets(bookingTickets);
  }, []);

  const handleChangeQuantity = (ticket: BookingTicket) => {
    const { areaCategoryCode } = ticket.ticket;
    const index = tickets.findIndex((t) => t.ticket.id === ticket.ticket.id);
    const bookingTicket = tickets[index];
    const totalTicketsInArea = selectedSeats.filter((s) => s.areaCategoryCode === areaCategoryCode).length;
    if (bookingTicket.quantity === totalTicketsInArea) return;
    bookingTicket.quantity += 1;
    let anotherBookingTickets = tickets.filter((t) => t.ticket.id !== ticket.ticket.id && t.ticket.areaCategoryCode === areaCategoryCode);
    let reduced = false;
    anotherBookingTickets = anotherBookingTickets.map((t) => {
      if (!reduced && t.quantity > 0) {
        t.quantity -= 1;
        reduced = true;
        return t;
      }
      return t;
    });
    reduced = false;
    const updatingTickets = [bookingTicket, ...anotherBookingTickets];
    const newTickets = [...tickets].map((t) => {
      const updated = updatingTickets.find((ut) => ut.ticket.id === t.ticket.id);
      if (updated) {
        return updated;
      }
      return t;
    });
    setTickets(newTickets);
  };

  const reduceTicketQuantity = (index: number) => {
    const newTickets = [...tickets];
    if (newTickets[index].quantity === 0) {
      reduceTicketQuantity(index - 1);
    } else {
      newTickets[index].quantity -= 1;
      setTickets(newTickets);
    }
  };

  const handleSeatSelect = (seat: MovieSessionSeat) => {
    const existingSeat = selectedSeats.find((s) => s.id === seat.id);

    if (existingSeat) {
      const index = tickets.findIndex((t) => t.ticket.areaCategoryCode === seat.areaCategoryCode && t.quantity > 0);
      reduceTicketQuantity(index);
      setSelectedSeats((prev) => prev.filter((s) => s.id !== seat.id));
    } else {
      const index = tickets.findIndex((t) => t.ticket.areaCategoryCode === seat.areaCategoryCode);
      if (selectedSeats.length === 10) {
        toast.warn('Та 10 хүртэлх суудал сонгох боломжтой');
        return;
      }
      setTickets((prev) => {
        const newTickets = [...prev];
        newTickets[index].quantity += 1;
        return newTickets;
      });
      setSelectedSeats((prev) => [...prev, seat]);
    }
  };

  const handleOrder = () => {
    const { email, phone, name } = formik.values;
    const newOrder = { ...orders };
    newOrder[userSession.sessionId] = { tickets, selectedSeats, userSession, email, phone, name };
    setOrders(newOrder);
    router.push(`/film/${movie.slug}/session/${session.id}/order?userSessionId=${userSession.sessionId}`);
  };

  const isFormikHasError = !formik.isValid || (Object.keys(formik.touched).length === 0 && formik.touched.constructor === Object);

  return (
    <>
      <Head>
        <title>{movie.title}</title>
        <meta content={movie.description} name="description" />
        <meta content={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${movie.horizontalPosterUrl}`} property="og:image" />
        <meta content={movie.title} property="og:title" />
        <meta content="video.movie" property="og:type" />
        <meta content={`${process.env.NEXT_PUBLIC_RESOURCE_URL}/film/${movie.slug}`} property="og:url" />
        <meta content={movie.description} property="og:description" />
        <FavIcon />
      </Head>
      <OffMenu menus={mainMenus} open={menuOpen} onClose={() => setMenuOpen(false)} />
      <NavigationBar menus={mainMenus} handleMenuOpen={() => setMenuOpen(true)} />
      <div className="h-[113px]" />
      <div className="container my-10">
        <Progress step={3} session={session} />
      </div>
      <div className="px-10">
        <div className="grid grid-cols-12 gap-6">
          <div className="flex flex-col col-span-12 gap-4 md:col-span-3">
            <div className="aspect-[3/4] relative">
              <Image
                src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${movie.verticalPosterUrl}`}
                width={300}
                height={400}
                alt={movie.title}
                className="absolute top-0 left-0 object-cover w-full h-full rounded-md"
              />
            </div>
            <h1 className="block w-full text-2xl font-medium text-center text-white group-hover:text-primary">{movie.title}</h1>

            <div className="flex flex-wrap gap-4">
              {movie.genres.map((genre) => (
                <Chip key={nanoid()} active>
                  {genre.genre.translated || genre.genre.title}
                </Chip>
              ))}
              <Chip className="bg-transparent" active>
                {movie.rating}
              </Chip>
            </div>
            <div className="flex flex-col gap-1">
              <p className="text-center text-gray-300">
                <span className="mr-3 font-bold text-white">Үргэлжлэх хугацаа:</span>
                {Math.floor(movie.duration / 60)} цаг {movie.duration % 60} минут
              </p>
              <p className="text-center text-gray-300">
                <span className="mr-3 font-bold text-white">Огноо:</span>
                {dayjs(session.showTime).format('YYYY-MM-DD HH:mm')}
              </p>
              <p className="text-center text-gray-300">
                <span className="mr-3 font-bold text-white">Танхим:</span>
                {session.screenName}
              </p>
            </div>
          </div>
          <div className="col-span-12 md:col-span-6">
            <div className="mb-8">
              <div className="w-full h-16 border-4 border-transparent border-t-white" style={{ borderRadius: '64px/50%' }}></div>
              <p className="-mt-12 text-center text-white uppercase">Дэлгэц</p>
            </div>
            <div>
              <SeatItems screenName={session.screenName} selectedSeats={selectedSeats} seats={seats} onSeatSelect={handleSeatSelect} />
            </div>
          </div>
          <div className="col-span-12 md:col-span-3">
            <div className="flex flex-col items-center gap-2">
              <div className="flex items-center gap-2 text-xl text-white">
                <IconTicket /> Тасалбар сонголт
              </div>
              <div className="mb-10 text-sm text-secondary">
                Сонгосон суудал: <span className="text-secondary/50">{totalTickets}</span>
              </div>
              {tickets.map((ticket) => (
                <div key={nanoid()} className="flex items-center justify-between w-full mb-4" style={{ userSelect: 'none' }}>
                  <div>
                    <div className="text-secondary">{ticket.ticket.title}</div>
                    <div className="text-sm text-secondary text-secondary/50">{currencyFormat(ticket.ticket.price)}</div>
                  </div>
                  <div className="flex items-center gap-3 text-white">
                    {ticket.quantity}
                    <Chip disabled={totalTickets === 0} onClick={() => handleChangeQuantity(ticket)} className="rounded-full">
                      <IconPlus />
                    </Chip>
                  </div>
                </div>
              ))}
              <div>
                <div className="flex justify-between gap-2 text-2xl">
                  <div className="text-secondary">Нийт:</div>
                  <div className="font-bold text-secondary">{currencyFormat(totalAmount)}</div>
                </div>
              </div>
              <form onSubmit={formik.handleSubmit} className="flex flex-col w-full gap-4">
                <TextField
                  icon={<IconUser />}
                  type="text"
                  label="Нэр"
                  onBlur={formik.handleBlur}
                  helperText={formik.touched.name ? formik.errors.name : undefined}
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  name="name"
                />
                <TextField
                  icon={<IconPhone />}
                  type="tel"
                  label="Утасны дугаар"
                  onBlur={formik.handleBlur}
                  helperText={formik.touched.phone ? formik.errors.phone : undefined}
                  value={formik.values.phone}
                  onChange={formik.handleChange}
                  name="phone"
                />
                <TextField
                  icon={<IconMail />}
                  type="email"
                  label="И-Мэйл"
                  onBlur={formik.handleBlur}
                  helperText={formik.touched.email ? formik.errors.email : undefined}
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  name="email"
                />
                <button type="submit">
                  <Chip disabled={isFormikHasError || totalTickets === 0} active className="block w-full py-1 mt-4 text-2xl text-center bg-transparent">
                    Захиалах
                  </Chip>
                </button>
              </form>
              <Countdown date={userSession.expireDate} />
            </div>
          </div>
        </div>
      </div>
      <Footer socialMenus={socialMenus} footMenus={footMenus} />
    </>
  );
}

export const getServerSideProps = (async (context) => {
  const { sessionId, slug } = context.params as { sessionId: string; slug: string };

  const session = await getSession(sessionId);
  const now = dayjs().add(30, 'minutes').toDate().getTime();
  const showTime = new Date(session.showTime).getTime();
  const doAired = now > showTime;
  if (doAired)
    return {
      notFound: true,
    };
  const movie = await getMovie(slug);
  if (!movie || !session) {
    return {
      notFound: true,
    };
  }
  const mainMenus = await getMenusByKey('main');
  const socialMenus = await getMenusByKey('social');
  const footMenus = await getMenusByKey('foot');
  const seatAreas = await getSeats(sessionId);
  const allSeats = seatAreas
    .map((area: MovieSessionArea) => area.seats)
    .reverse()
    .flat();
  const seats = allSeats.map((row: any) => row.reverse()).reverse();

  const userSession = await createUserSession();

  const repo: Repo = { mainMenus, socialMenus, footMenus, movie, session, userSession, seats };
  return { props: { repo } };
}) satisfies GetServerSideProps<{ repo: Repo }>;
