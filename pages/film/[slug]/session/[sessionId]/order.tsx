import { Footer } from '@/components/shared/footer';
import { NavigationBar } from '@/components/shared/navigation-bar';
import { getMovie } from '@/services/getMovie';
import { getMenusByKey } from '@/services/menu';
import { FullMovie, FullSession, MovieSessionArea, MovieSessionSeat, OrderResponse } from '@/types';
import { Menu } from '@prisma/client';
import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import { nanoid } from 'nanoid';
import { Chip } from '@/components/shared/chip';
import { useEffect, useRef, useState } from 'react';
import { OffMenu } from '@/components/shared/off-menu';
import { FavIcon } from '@/components/shared/fav';
import { getSession } from '@/services/getSession';
import dayjs from 'dayjs';
import { getSeats } from '@/services/getSeats';
import { IconTicket } from '@tabler/icons-react';
import { currencyFormat } from '@/utils/currency-formatter';
import { Progress } from '@/components/shared/progress';
import { useOrder } from '@/hooks/useOrder';
import { Countdown } from '@/components/shared/countdown';
import axios from 'axios';
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import Link from 'next/link';
import { checkQpayBySenderNo } from '@/services/qpay';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';
import { Loader } from '@/components/loader';

type Repo = {
  mainMenus: Menu[];
  socialMenus: Menu[];
  footMenus: Menu[];
  movie: FullMovie;
  session: FullSession;
  seatAreas: MovieSessionArea[];
  userSessionId: string;
};

const bankImagePath: { [key: string]: string } = {
  'qPay wallet': '/bank/bank_qpay.webp',
  'Khan bank': '/bank/bank_khan.webp',
  'State bank 3.0': '/bank/bank_state.webp',
  'Xac bank': '/bank/bank_xac.webp',
  'Trade and Development bank': '/bank/bank_tdb.webp',
  'Social Pay': '/bank/bank_socialpay.webp',
  'Most money': '/bank/bank_mostmoney.webp',
  'National investment bank': '/bank/bank_ni.webp',
  'Chinggis khaan bank': '/bank/bank_chinggis.webp',
  'Capitron bank': '/bank/bank_capitron.webp',
  'Bogd bank': '/bank/bank_bogd.webp',
  'Trans bank': '/bank/bank_trans.webp',
  'M bank': '/bank/bank_mbank.webp',
  'Ard App': '/bank/bank_ard.webp',
  'Arig bank': '/bank/bank_arig.webp',
  Monpay: '/bank/bank_monpay.webp',
};

export default function Page({ repo }: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { mainMenus, movie, socialMenus, session, footMenus, userSessionId } = repo;
  const intervalRef = useRef<NodeJS.Timeout>();
  const { orders, loading: ordersLoading } = useOrder();
  const [loading, setLoading] = useState(true);
  const [menuOpen, setMenuOpen] = useState(false);
  const [orderResponse, setOrderResponse] = useState<OrderResponse>();
  const router = useRouter();

  const isLoading = loading || ordersLoading;

  const order = orders[userSessionId];

  const clearPoll = () => {
    if (intervalRef.current) {
      clearInterval(intervalRef.current);
    }
  };

  useEffect(() => {
    return () => {
      clearPoll();
    };
  }, []);

  useEffect(() => {
    if (orderResponse) {
      intervalRef.current = setInterval(async () => {
        const response = await checkQpayBySenderNo(orderResponse.qpay.senderNo);
        if (response) {
          clearPoll();
          router.push(`/film/${movie.slug}/session/${session.id}/order-success?userSessionId=${userSessionId}`);
          toast.success('Захиалга амжилттай боллоо!');
        }
      }, 3000);
    }
  }, [orderResponse]);

  useEffect(() => {
    if (!orderResponse) {
      axios
        .post(
          `https://www.tengis.mn/api/movies/sessions/order/create`,
          { sessionId: session.id, userSessionId, selectedSeats: order.selectedSeats, tickets: order.tickets, name: order.name, email: order.email, phone: order.phone },
          { headers: { Authorization: 'Bearer ' + order.userSession.accessToken } }
        )
        .then((res) => {
          if (res.data.error) {
            toast.error('Суудал захиалахад алдаа гарлаа');
            router.push(`/film/${movie.slug}/session/${session.id}`);
          } else {
            setOrderResponse(res.data.result);
            setLoading(false);
          }
        })
        .catch((err) => {
          toast.error('Суудал захиалахад алдаа гарлаа');
          router.push(`/film/${movie.slug}/session/${session.id}`);
          console.error(err);
        });
    }
  }, [orderResponse]);

  const totalTickets = order?.tickets.reduce((acc, ticket) => acc + ticket.quantity, 0) || 0;
  const totalAmount = order?.tickets.reduce((acc, ticket) => acc + ticket.quantity * Number(ticket.ticket.price), 0) || 0;

  let seatIndex = 0;

  const getTicketSeats = (quantity: number) => {
    const seats: MovieSessionSeat[] = [];
    for (let i = 0; i < quantity; i++) {
      seats.push(order.selectedSeats[seatIndex]);
      seatIndex++;
    }
    return seats;
  };
  if (loading) return <Loader />;

  return (
    <>
      <Head>
        <title>{movie.title}</title>
        <meta content={movie.description} name="description" />
        <meta content={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${movie.horizontalPosterUrl}`} property="og:image" />
        <meta content={movie.title} property="og:title" />
        <meta content="video.movie" property="og:type" />
        <meta content={`${process.env.NEXT_PUBLIC_RESOURCE_URL}/film/${movie.slug}`} property="og:url" />
        <meta content={movie.description} property="og:description" />
        <FavIcon />
      </Head>
      <OffMenu menus={mainMenus} open={menuOpen} onClose={() => setMenuOpen(false)} />
      <NavigationBar menus={mainMenus} handleMenuOpen={() => setMenuOpen(true)} />
      <div className="h-[113px]" />
      <div className="container my-10">
        <Progress step={4} session={session} />
        <div className="grid grid-cols-12 gap-6">
          <div className="flex flex-col col-span-12 gap-4 md:col-span-2">
            <div className="aspect-[3/4] relative">
              <Image
                src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${movie.verticalPosterUrl}`}
                width={300}
                height={400}
                alt={movie.title}
                className="absolute top-0 left-0 object-cover w-full h-full rounded-md"
              />
            </div>
            <h1 className="block w-full text-2xl font-medium text-center text-white group-hover:text-primary">{movie.title}</h1>

            <div className="flex flex-wrap gap-4">
              {movie.genres.map((genre) => (
                <Chip key={nanoid()} active>
                  {genre.genre.translated || genre.genre.title}
                </Chip>
              ))}
              <Chip className="bg-transparent" active>
                {movie.rating}
              </Chip>
            </div>
            <div className="flex flex-col gap-1">
              <p className="text-center text-gray-300">
                <span className="mr-3 font-bold text-white">Үргэлжлэх хугацаа:</span>
                {Math.floor(movie.duration / 60)} цаг {movie.duration % 60} минут
              </p>
              <p className="text-center text-gray-300">
                <span className="mr-3 font-bold text-white">Огноо:</span>
                {dayjs(session.showTime).format('YYYY-MM-DD HH:mm')}
              </p>
              <p className="text-center text-gray-300">
                <span className="mr-3 font-bold text-white">Танхим:</span>
                {session.screenName}
              </p>
            </div>
          </div>
          <div className="col-span-12 my-5 md:col-span-7">
            {orderResponse && orderResponse.qpay.response && (
              <div className="flex flex-col items-center justify-center w-full gap-5">
                <Image src={`data:image/png;base64,${orderResponse.qpay.response.qrImage}`} width={200} height={200} alt={session.movie.title} />
                <div className="grid justify-center grid-cols-3 gap-5 lg:grid-cols-8 md:grid-cols-6 place-items-center">
                  {orderResponse.qpay.response.urls.map((url) => (
                    <Link key={url.id} href={url.link} target="_blank" className="block overflow-hidden rounded-md aspect-square">
                      <Image width={150} height={150} src={bankImagePath[url.name]} alt={url.name} className="w-full h-full" />
                    </Link>
                  ))}
                </div>
              </div>
            )}
          </div>
          <div className="col-span-12 md:col-span-3">
            <div className="flex flex-col items-center gap-2">
              <div className="flex items-center gap-2 text-xl text-white">
                <IconTicket /> Тасалбар сонголт
              </div>
              <div className="mb-10 text-sm text-secondary">
                Сонгосон суудал: <span className="text-secondary/50">{isLoading ? <Skeleton inline width={20} height={20} /> : <>{totalTickets}</>}</span>
              </div>
              {order.tickets
                .filter((ticket) => ticket.quantity > 0)
                .map((ticket) => (
                  <div key={nanoid()} className="w-full">
                    <div className="flex items-center justify-between w-full mb-4" style={{ userSelect: 'none' }}>
                      <div>
                        <div className="text-secondary">{ticket.ticket.title}</div>
                        <div className="text-sm text-secondary text-secondary/50">{currencyFormat(ticket.ticket.price)}</div>
                      </div>
                      <div className="text-white ">{ticket.quantity}</div>
                    </div>
                    <div>
                      <ul className="flex flex-col gap-2 pl-6 list-disc">
                        {getTicketSeats(ticket.quantity).map((seat) => (
                          <li key={nanoid()} className="text-sm text-secondary text-secondary/50 ">
                            <span>Эгнээ</span> {seat.rowName} - <span>Суудал</span> {seat.seatName}
                          </li>
                        ))}
                      </ul>
                    </div>
                  </div>
                ))}
              <div>
                <div className="flex justify-between gap-2 text-2xl">
                  <div className="text-secondary">Нийт:</div>
                  <div className="font-bold text-secondary">{currencyFormat(totalAmount)}</div>
                </div>
              </div>
              <Countdown date={order.userSession?.expireDate} />
            </div>
          </div>
        </div>
      </div>
      <Footer socialMenus={socialMenus} footMenus={footMenus} />
    </>
  );
}

export const getServerSideProps = (async (context) => {
  const { sessionId, slug, userSessionId } = context.query as { sessionId: string; slug: string; userSessionId: string };

  const session = await getSession(sessionId);
  const movie = await getMovie(slug);
  if (!movie || !session) {
    return {
      notFound: true,
    };
  }
  if ((session as FullSession).status !== 'ENABLED' || (movie as FullMovie).status !== 'ENABLED') {
    return {
      notFound: true,
    };
  }
  const mainMenus = await getMenusByKey('main');
  const socialMenus = await getMenusByKey('social');
  const footMenus = await getMenusByKey('foot');
  const seatAreas = await getSeats(sessionId);
  const repo: Repo = { mainMenus, socialMenus, footMenus, movie, session, seatAreas, userSessionId };
  return { props: { repo } };
}) satisfies GetServerSideProps<{ repo: Repo }>;
