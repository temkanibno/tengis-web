import { Footer } from '@/components/shared/footer';
import { NavigationBar } from '@/components/shared/navigation-bar';
import { getMovie } from '@/services/getMovie';
import { getMenusByKey } from '@/services/menu';
import { FullMovie, FullSession, MovieSessionArea, MovieSessionSeat, OrderResponse } from '@/types';
import { Menu } from '@prisma/client';
import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import { nanoid } from 'nanoid';
import { Chip } from '@/components/shared/chip';
import { useEffect, useState } from 'react';
import { OffMenu } from '@/components/shared/off-menu';
import { FavIcon } from '@/components/shared/fav';
import { getSession } from '@/services/getSession';
import dayjs from 'dayjs';
import { getSeats } from '@/services/getSeats';
import { IconTicket } from '@tabler/icons-react';
import { currencyFormat } from '@/utils/currency-formatter';
import { Progress } from '@/components/shared/progress';
import { useOrder } from '@/hooks/useOrder';
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import { SuccessIcon } from '@/components/success';
import { Loader } from '@/components/loader';
import { FailIcon } from '@/components/fail';
import Link from 'next/link';

type Repo = {
  mainMenus: Menu[];
  socialMenus: Menu[];
  footMenus: Menu[];
  movie: FullMovie;
  session: FullSession;
  seatAreas: MovieSessionArea[];
  userSessionId: string;
};

export default function Page({ repo }: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { mainMenus, movie, socialMenus, session, footMenus, userSessionId } = repo;
  const { orders, loading: ordersLoading, finishOrder } = useOrder();
  const [savedOrder, setSavedOrder] = useState<any>(null);
  const [menuOpen, setMenuOpen] = useState(false);

  // const isLoading = loading || ordersLoading;
  const isLoading = ordersLoading;

  const order = orders[userSessionId];

  useEffect(() => {
    finishOrder(userSessionId).then((res: any) => {
      setSavedOrder(res);
    });
  }, []);

  const totalTickets = order?.tickets.reduce((acc, ticket) => acc + ticket.quantity, 0) || 0;
  const totalAmount = order?.tickets.reduce((acc, ticket) => acc + ticket.quantity * Number(ticket.ticket.price), 0) || 0;

  let seatIndex = 0;

  const getTicketSeats = (quantity: number) => {
    const seats: MovieSessionSeat[] = [];
    for (let i = 0; i < quantity; i++) {
      seats.push(order.selectedSeats[seatIndex]);
      seatIndex++;
    }
    return seats;
  };
  if (ordersLoading) return <Loader />;

  return (
    <>
      <Head>
        <title>{movie.title}</title>
        <meta content={movie.description} name="description" />
        <meta content={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${movie.horizontalPosterUrl}`} property="og:image" />
        <meta content={movie.title} property="og:title" />
        <meta content="video.movie" property="og:type" />
        <meta content={`${process.env.NEXT_PUBLIC_RESOURCE_URL}/film/${movie.slug}`} property="og:url" />
        <meta content={movie.description} property="og:description" />
        <FavIcon />
      </Head>
      <OffMenu menus={mainMenus} open={menuOpen} onClose={() => setMenuOpen(false)} />
      <NavigationBar menus={mainMenus} handleMenuOpen={() => setMenuOpen(true)} />
      <div className="h-[113px]" />
      <div className="container my-10">
        <Progress step={5} session={session} />
        <div className="grid grid-cols-12 gap-6">
          <div className="flex flex-col col-span-12 gap-4 md:col-span-2">
            <div className="aspect-[3/4] relative">
              <Image
                src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${movie.verticalPosterUrl}`}
                width={300}
                height={400}
                alt={movie.title}
                className="absolute top-0 left-0 object-cover w-full h-full rounded-md"
              />
            </div>
            <h1 className="block w-full text-2xl font-medium text-center text-white group-hover:text-primary">{movie.title}</h1>

            <div className="flex flex-wrap gap-4">
              {movie.genres.map((genre) => (
                <Chip key={nanoid()} active>
                  {genre.genre.translated || genre.genre.title}
                </Chip>
              ))}
              <Chip className="bg-transparent" active>
                {movie.rating}
              </Chip>
            </div>
            <div className="flex flex-col gap-1">
              <p className="text-center text-gray-300">
                <span className="mr-3 font-bold text-white">Үргэлжлэх хугацаа:</span>
                {Math.floor(movie.duration / 60)} цаг {movie.duration % 60} минут
              </p>
              <p className="text-center text-gray-300">
                <span className="mr-3 font-bold text-white">Огноо:</span>
                {dayjs(session.showTime).format('YYYY-MM-DD HH:mm')}
              </p>
              <p className="text-center text-gray-300">
                <span className="mr-3 font-bold text-white">Танхим:</span>
                {session.screenName}
              </p>
            </div>
          </div>
          <div className="col-span-12 my-5 md:col-span-7">
            <div className="flex flex-col items-center justify-center w-full gap-5 my-10">
              {savedOrder && (
                <>
                  {savedOrder.status === 'FAILED' ? (
                    <>
                      <FailIcon />
                      <h1 className="text-2xl font-bold text-center text-white">Таны захиалга амжилтгүй боллоо</h1>
                      <p className="mx-10 text-xl text-center text-white">
                        Таны төлбөр буцааж олгогдлох болно.
                        <br /> Та захиалгаа шинээр үүсгэнэ үү
                      </p>
                      <div className="flex justify-center">
                        <Link href={`/film/${session.movie.slug}/session/${session.id}`}>
                          <Chip active>Шинээр үүсгэх</Chip>
                        </Link>
                      </div>
                    </>
                  ) : (
                    <>
                      <SuccessIcon />
                      <h1 className="text-2xl font-bold text-center text-white">Таны захиалга амжилттай боллоо</h1>
                      <p className="mx-10 text-xl text-center text-white">
                        Таны захиалгатай холбоотой мэдээллийг <b className="font-bold">{order?.email}</b> холбоосруу илгээлээ.
                      </p>
                      <div className="mx-10 text-center text-white">
                        {/* eslint-disable-next-line @next/next/no-img-element*/}
                        <img src={`data:image/png;base64, ${savedOrder.result.bookingQr}`} alt="" width={200} height={200} />
                        <p>
                          {savedOrder.result.bookingId} | {savedOrder.result.bookingNumber}
                        </p>
                      </div>
                      <div className="mx-10 text-white my">
                        <p className="mb-4 text-center">Анхааруулга: Тасалбараа дээрх кодоор Kiosk-с авна уу.</p>
                        <p className="mb-4 text-center">
                          <b className="font-bold">Хэрвээ та урьдчилсан тасалбар захиалга хийсэн бол та дараах болзолыг хангасан байна.</b>
                        </p>
                        <ol className="list-decimal">
                          <li>Энэхүү хуудсыг баримт болгон хэвлэмэл байдлаар авчирч болно.</li>
                          <li>Энэхүү системийг ашиглаж тасалбар захиалга хийсэн бол үйлчилгээний нөхцөлийг хүлээн зөвшөөрсөн гэж үзнэ.</li>
                          <li>Тухайн кино үзвэр насны ангилал заасны дагуу үзвэрийн насны хүмүүсийг оруулна.</li>
                        </ol>
                      </div>
                    </>
                  )}
                </>
              )}
            </div>
          </div>
          <div className="col-span-12 md:col-span-3">
            <div className="flex flex-col items-center gap-2">
              <div className="flex items-center gap-2 text-xl text-white">
                <IconTicket /> Тасалбар сонголт
              </div>
              <div className="mb-10 text-sm text-secondary">
                Сонгосон суудал: <span className="text-secondary/50">{isLoading ? <Skeleton inline width={20} height={20} /> : <>{totalTickets}</>}</span>
              </div>
              {order.tickets
                .filter((ticket) => ticket.quantity > 0)
                .map((ticket) => (
                  <div key={nanoid()} className="w-full">
                    <div className="flex items-center justify-between w-full mb-4" style={{ userSelect: 'none' }}>
                      <div>
                        <div className="text-secondary">{ticket.ticket.title}</div>
                        <div className="text-sm text-secondary text-secondary/50">{currencyFormat(ticket.ticket.price)}</div>
                      </div>
                      <div className="text-white ">{ticket.quantity}</div>
                    </div>
                    <div>
                      <ul className="flex flex-col gap-2 pl-6 list-disc">
                        {getTicketSeats(ticket.quantity).map((seat) => (
                          <li key={nanoid()} className="text-sm text-secondary text-secondary/50 ">
                            <span>Эгнээ</span> {seat.rowName} - <span>Суудал</span> {seat.seatName}
                          </li>
                        ))}
                      </ul>
                    </div>
                  </div>
                ))}
              <div>
                <div className="flex justify-between gap-2 text-2xl">
                  <div className="text-secondary">Нийт:</div>
                  <div className="font-bold text-secondary">{currencyFormat(totalAmount)}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer socialMenus={socialMenus} footMenus={footMenus} />
    </>
  );
}

export const getServerSideProps = (async (context) => {
  const { sessionId, slug, userSessionId } = context.query as { sessionId: string; slug: string; userSessionId: string };

  const session = await getSession(sessionId);
  const movie = await getMovie(slug);
  if (!movie || !session) {
    return {
      notFound: true,
    };
  }
  if ((session as FullSession).status !== 'ENABLED' || (movie as FullMovie).status !== 'ENABLED') {
    return {
      notFound: true,
    };
  }
  const mainMenus = await getMenusByKey('main');
  const socialMenus = await getMenusByKey('social');
  const footMenus = await getMenusByKey('foot');
  const seatAreas = await getSeats(sessionId);
  const repo: Repo = { mainMenus, socialMenus, footMenus, movie, session, seatAreas, userSessionId };
  return { props: { repo } };
}) satisfies GetServerSideProps<{ repo: Repo }>;
