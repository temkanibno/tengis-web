import { Footer } from '@/components/shared/footer';
import { NavigationBar } from '@/components/shared/navigation-bar';
import { getMovie } from '@/services/getMovie';
import { getMenusByKey } from '@/services/menu';
import { FullMovie, MovieAttribute } from '@/types';
import { Menu, MovieSession } from '@prisma/client';
import { GetStaticProps, GetStaticPaths, InferGetStaticPropsType } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import { nanoid } from 'nanoid';
import { getDayText } from '@/utils/getDayText';
import { Chip } from '@/components/shared/chip';
import dayjs from 'dayjs';
import Link from 'next/link';
import { getMovies } from '@/services/getMovies';
import { useState } from 'react';
import { IconPlayerPlayFilled, IconVip } from '@tabler/icons-react';
import { OffMenu } from '@/components/shared/off-menu';
import { FavIcon } from '@/components/shared/fav';

type Repo = {
  movies: FullMovie[];
  mainMenus: Menu[];
  socialMenus: Menu[];
  footMenus: Menu[];
  movie: FullMovie;
};

export default function Page({ repo }: InferGetStaticPropsType<typeof getStaticProps>) {
  const { mainMenus, movie, movies, socialMenus, footMenus } = repo;
  const [videoPlaying, setVideoPlaying] = useState(false);
  const attributes = movie.attributes as MovieAttribute[];
  const [menuOpen, setMenuOpen] = useState(false);

  const getDetailLink = (sessionId: string) => {
    return `/film/${movie.slug}/session/${sessionId}`;
  };

  let sessionsMap = new Map<string, MovieSession[]>();

  movie.sessions.forEach((item: MovieSession) => {
    if (!item.isDeleted) {
      const sessionDate = dayjs(item.showTime);
      let formattedDay = sessionDate.format('YYYY-MM-DD');
      if (sessionDate.hour() < 3) {
        formattedDay = sessionDate.subtract(1, 'day').format('YYYY-MM-DD');
      }

      if (sessionsMap.has(formattedDay)) {
        sessionsMap.get(formattedDay)?.push(item);
      } else {
        sessionsMap.set(formattedDay, [item]);
      }
    }
  });

  const entries = Array.from(sessionsMap.entries());

  return (
    <>
      <Head>
        <title>{movie.title}</title>
        <meta content={movie.description} name="description" />
        <meta content={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${movie.horizontalPosterUrl}`} property="og:image" />
        <meta content={movie.title} property="og:title" />
        <meta content="video.movie" property="og:type" />
        <meta content={`${process.env.NEXT_PUBLIC_RESOURCE_URL}/film/${movie.slug}`} property="og:url" />
        <meta content={movie.description} property="og:description" />
        <FavIcon />
      </Head>
      <OffMenu menus={mainMenus} open={menuOpen} onClose={() => setMenuOpen(false)} />
      <NavigationBar menus={mainMenus} handleMenuOpen={() => setMenuOpen(true)} />
      <div className="h-[113px]" />
      <div className="container my-10">
        <div className="grid grid-cols-12 gap-6">
          <div className="col-span-12 md:col-span-8">
            <div>
              <div className="relative mb-6 overflow-hidden aspect-video">
                {!videoPlaying && (
                  <div onClick={() => setVideoPlaying(true)} className="cursor-pointer">
                    <Image
                      src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${movie.horizontalPosterUrl}`}
                      width={1080}
                      height={607.5}
                      alt={movie.title}
                      className="absolute top-0 left-0 object-cover w-full h-full"
                    />
                    <div className="absolute grid w-24 h-24 text-5xl -translate-x-1/2 -translate-y-1/2 border-2 rounded-full bg-white/50 left-1/2 top-1/2 place-items-center text-black/60">
                      <IconPlayerPlayFilled />
                    </div>
                  </div>
                )}
                {videoPlaying && (
                  <div>
                    <iframe
                      className="absolute top-0 left-0 w-full h-full"
                      src={`https://www.facebook.com/plugins/video.php?href=${encodeURIComponent(movie.trailer + '')}&show_text=0&width=1080`}
                      width="1080"
                      height="608"
                    />
                  </div>
                )}
              </div>
              <h1 className="mb-6 text-6xl text-center text-white">{movie.title}</h1>
              <div className="grid grid-cols-2 gap-8 mb-6">
                <div className="col-span-2 md:col-span-1">
                  <div className="aspect-[3/4] relative overflow-hidden">
                    <Image
                      src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${movie.verticalPosterUrl}`}
                      width={300}
                      height={400}
                      alt={movie.title}
                      className="absolute top-0 left-0 object-cover w-full h-full"
                    />
                    <div className="absolute left-4 top-4">
                      <Chip active>{movie.rating}</Chip>
                    </div>
                    <div className="absolute flex gap-4 left-4 bottom-4">
                      {movie.genres.map((genre: any) => (
                        <Chip key={nanoid()} active>
                          {genre.genre.translated || genre.genre.title}
                        </Chip>
                      ))}
                    </div>
                  </div>
                </div>
                <div className="col-span-2 md:col-span-1">
                  <ul className="flex flex-col gap-4">
                    {entries.map(([day, sessions]) => (
                      <li key={nanoid()}>
                        <p className="mb-4 text-xl text-white">{getDayText(day)}</p>
                        <div className="flex flex-wrap gap-4">
                          {sessions.map((session: MovieSession) => {
                            if (session.isVip) return null;
                            const now = dayjs().add(30, 'minutes').toDate().getTime();
                            const showTime = new Date(session.showTime).getTime();
                            const doAired = now > showTime;
                            if (!doAired && session.status === 'DELETED') return null;
                            if (doAired) {
                              return (
                                <Chip key={nanoid()} disabled={doAired}>
                                  {dayjs(session.showTime).format('HH:mm')}
                                </Chip>
                              );
                            } else {
                              return (
                                <Link href={getDetailLink(session.id)} key={nanoid()}>
                                  <Chip disabled={doAired}>{dayjs(session.showTime).format('HH:mm')}</Chip>
                                </Link>
                              );
                            }
                          })}
                        </div>
                        <div className="flex flex-wrap gap-4">
                          {sessions.map((session: MovieSession) => {
                            if (!session.isVip) return null;
                            const now = dayjs().add(30, 'minutes').toDate().getTime();
                            const showTime = new Date(session.showTime).getTime();
                            const doAired = now > showTime;
                            if (doAired) {
                              return (
                                <Chip key={nanoid()} disabled={doAired} className="mt-4 bg-transparent">
                                  {dayjs(session.showTime).format('HH:mm')} <IconVip />
                                </Chip>
                              );
                            } else {
                              return (
                                <Link href={getDetailLink(session.id)} key={nanoid()}>
                                  <Chip disabled={doAired} key={nanoid()} active className="mt-4 bg-transparent">
                                    {dayjs(session.showTime).format('HH:mm')} <IconVip />
                                  </Chip>
                                </Link>
                              );
                            }
                          })}
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
              <div>
                <ul className="flex flex-col gap-2">
                  <li>
                    <p className="text-gray-300">
                      <span className="mr-3 font-bold text-white">Үргэлжлэх хугацаа:</span>
                      {Math.floor(movie.duration / 60)} цаг {movie.duration % 60} минут
                    </p>
                  </li>
                  <li>
                    <p className="text-gray-300">
                      <span className="mr-3 font-bold text-white">Нээлтийн огноо:</span>
                      {dayjs(movie.nationalOpenDate).format('YYYY-MM-DD')}
                    </p>
                  </li>
                  {attributes.map((attr) => (
                    <li key={nanoid()}>
                      <p className="text-gray-300">
                        <span className="mr-3 font-bold text-white">{attr.Description}</span>
                        {attr.Url}
                      </p>
                    </li>
                  ))}

                  <li>
                    <p className="text-gray-300">
                      <span className="mr-3 font-bold text-white">Танилцуулга:</span>
                      {movie.description}
                    </p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-span-12 md:col-span-4">
            <div className="grid grid-cols-2 gap-4">
              {movies.map((item: FullMovie) => (
                <Link href={`/film/${item.slug}`} key={nanoid()} title={item.title}>
                  <div className="aspect-[3/4] relative">
                    <Image
                      src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${item.verticalPosterUrl}`}
                      width={300}
                      height={400}
                      alt={item.title}
                      className="absolute top-0 left-0 object-cover w-full h-full rounded-md"
                    />
                  </div>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
      <Footer socialMenus={socialMenus} footMenus={footMenus} />
    </>
  );
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { slug } = context.params as { slug: string };

  const mainMenus = await getMenusByKey('main');
  const socialMenus = await getMenusByKey('social');
  const footMenus = await getMenusByKey('foot');
  const movie = await getMovie(slug);

  if (!movie) {
    return {
      notFound: true,
      revalidate: 60 * 5,
    };
  }

  const movies = await getMovies({ type: 'ONGOING', orderType: 'asc' });
  const movieIndex = movies.findIndex((item: FullMovie) => item.slug === slug);
  movies.splice(movieIndex, 1);

  const repo: Repo = { mainMenus, movies, socialMenus, footMenus, movie };

  return {
    props: { repo },
    revalidate: 60 * 5,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const onGoingMovies = await getMovies({ type: 'ONGOING', orderType: 'asc' });
  const upComingMovies = await getMovies({ type: 'UPCOMING', orderType: 'asc' });
  const preOrderingMovies = await getMovies({ type: 'PREORDERING', orderType: 'asc' });
  const movies = [...onGoingMovies, ...upComingMovies, ...preOrderingMovies];

  const paths = movies.map((movie: FullMovie) => ({
    params: { slug: movie.slug },
  }));

  return {
    paths,
    fallback: 'blocking',
  };
};
