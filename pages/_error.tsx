import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

export default function Page() {
  const router = useRouter();
  useEffect(() => {
    router.replace('/error');
  }, [router]);
  return (
    <div className="aspect-video relative">
      <Image src={'/static/404.webp'} alt={'Not found'} width={1080} height={678} className="absolute top-0 left-0 w-full h-full" />
      <div className="absolute top-0 left-0 w-full h-full bg-black/50 grid place-items-center">
        <div className="text-center">
          <h2 className="text-white text-2xl mb-6">Уучлаарай</h2>
          <h1 className="text-white text-4xl mb-6">Алдаа гарлаа!</h1>
          <Link href={'/'} className="text-blue-600 hover:text-blue-500">
            Нүүр хуудас
          </Link>
        </div>
      </div>
    </div>
  );
}
