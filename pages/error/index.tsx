import { Footer } from '@/components/shared/footer';
import { NavbarPlaceholder } from '@/components/shared/navbar-plaholder';
import { NavigationBar } from '@/components/shared/navigation-bar';
import { OffMenu } from '@/components/shared/off-menu';
import { getMovies } from '@/services/getMovies';
import { getMenusByKey } from '@/services/menu';
import { FullMovie } from '@/types';
import { Menu } from '@prisma/client';
import { GetStaticProps, InferGetStaticPropsType } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import { nanoid } from 'nanoid';
import { FavIcon } from '@/components/shared/fav';

type Repo = {
  mainMenus: Menu[];
  socialMenus: Menu[];
  footMenus: Menu[];
  movies: FullMovie[];
};

export default function Page({ repo }: InferGetStaticPropsType<typeof getStaticProps>) {
  const { mainMenus, socialMenus, movies, footMenus } = repo;
  const [menuOpen, setMenuOpen] = useState(false);

  return (
    <>
      <Head>
        <title>Tengis Movie Theater .::. Тэнгис кино теарт</title>
        <meta property="og:site_name" content="www.tengis.mn" />
        <meta property="og:title" content="Tengis Movie Theater | Тэнгис кино теарт" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://www.tengis.mn/" />
        <meta property="og:image" content="https://www.tengis.mn/static/tengis.jpg" />
        <meta
          property="og:description"
          content="“Тэнгис” кино театр нь кино үзвэрийн хамгийн сүүлийн үеийн техникээр иж бүрэн тоноглогдсон үзвэрийн 4-н танхимтай бөгөөд нэгэн зэрэг 1160-н хүнд үйлчлэх хүчин чадалтай, монгол болон дэлхийн бусад орнуудын шилдэг кинонуудыг 35 мм-ийн кино хальс ба 3D технологийг ашиглан үзэгчдэд хүргэдэг."
        />
        <meta
          name="description"
          content="“Тэнгис” кино театр нь кино үзвэрийн хамгийн сүүлийн үеийн техникээр иж бүрэн тоноглогдсон үзвэрийн 4-н танхимтай бөгөөд нэгэн зэрэг 1160-н хүнд үйлчлэх хүчин чадалтай, монгол болон дэлхийн бусад орнуудын шилдэг кинонуудыг 35 мм-ийн кино хальс ба 3D технологийг ашиглан үзэгчдэд хүргэдэг."
        />
        <FavIcon />
      </Head>
      <OffMenu menus={mainMenus} open={menuOpen} onClose={() => setMenuOpen(false)} />
      <NavigationBar menus={mainMenus} handleMenuOpen={() => setMenuOpen(true)} />
      <NavbarPlaceholder />
      <div className="container my-10">
        <div className="grid grid-cols-12 gap-6">
          <div className="col-span-12 ">
            <div className="relative aspect-video">
              <Image src={'/static/404.webp'} alt={'Not found'} width={1080} height={678} className="absolute top-0 left-0 w-full h-full" />
              <div className="absolute top-0 left-0 grid w-full h-full bg-black/50 place-items-center">
                <div className="text-center">
                  <h2 className="mb-6 text-2xl text-white">Уучлаарай</h2>
                  <h1 className="mb-6 text-4xl text-white">Алдаа гарлаа!</h1>
                  <Link href={'/'} className="text-blue-600 hover:text-blue-500">
                    Нүүр хуудас
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-12">
            <div className="grid content-center grid-cols-2 gap-4 md:grid-cols-6 sm:grid-cols-3">
              {movies.map((item: FullMovie) => (
                <Link href={`/film/${item.slug}`} key={nanoid()} title={item.title}>
                  <div className="aspect-[3/4] relative">
                    <Image
                      src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${item.verticalPosterUrl}`}
                      width={300}
                      height={400}
                      alt={item.title}
                      className="absolute top-0 left-0 object-cover w-full h-full rounded-md"
                    />
                  </div>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
      <Footer socialMenus={socialMenus} footMenus={footMenus} />
    </>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const movies = await getMovies({ type: 'ONGOING', orderType: 'asc' });
  const mainMenus = await getMenusByKey('main');
  const socialMenus = await getMenusByKey('social');
  const footMenus = await getMenusByKey('foot');

  const repo: Repo = { mainMenus, movies, socialMenus, footMenus };

  return {
    props: { repo },
    revalidate: 60 * 10,
  };
};
