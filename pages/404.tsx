import Image from 'next/image';
import Link from 'next/link';

export default function Page() {
  return (
    <div className="relative w-full h-screen">
      <Image src={'/static/404.webp'} alt={'Not found'} width={1080} height={678} className="absolute top-0 left-0 w-full h-full" />
      <div className="absolute top-0 left-0 grid w-full h-full bg-black/50 place-items-center">
        <div className="text-center">
          <h2 className="mb-6 text-2xl text-white">Уучлаарай</h2>
          <h1 className="mb-6 text-4xl text-white">Хуудас олдсонгүй!</h1>
          <Link href={'/'} className="text-blue-600 hover:text-blue-500">
            Нүүр хуудас
          </Link>
        </div>
      </div>
    </div>
  );
}
