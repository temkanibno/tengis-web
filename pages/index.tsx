import { Carousels } from '@/components/carousels';
import { DayPicker } from '@/components/home/day-picker';
import { GameSection } from '@/components/home/game-section';
import { MoviesSection } from '@/components/home/movies';
import { MoviesSectionBig } from '@/components/home/movies-big';
import { PriceSection } from '@/components/home/price-section';
import { FavIcon } from '@/components/shared/fav';
import { Footer } from '@/components/shared/footer';
import { NavigationBar } from '@/components/shared/navigation-bar';
import { OffMenu } from '@/components/shared/off-menu';
import { getCarousels } from '@/services/carousels';
import { getDays } from '@/services/getDays';
import { getMovies } from '@/services/getMovies';
import { getMenusByKey } from '@/services/menu';
import { FullMovie } from '@/types';
import { Carousel, Menu } from '@prisma/client';
import { InferGetStaticPropsType } from 'next';

import Head from 'next/head';
import { useState } from 'react';

type Repo = {
  mainMenus: Menu[];
  socialMenus: Menu[];
  footMenus: Menu[];
  ongoingsMap: { [key: string]: FullMovie[] };
  preorderings: FullMovie[];
  upcomings: FullMovie[];
  carousels: Carousel[];
  days: string[];
};

export default function Page({ repo }: InferGetStaticPropsType<typeof getStaticProps>) {
  const { mainMenus, socialMenus, footMenus, ongoingsMap, preorderings, upcomings, carousels, days } = repo;
  const [day, setDay] = useState(days[0]);
  const [menuOpen, setMenuOpen] = useState(false);
  const ongoings = ongoingsMap[day];
  return (
    <div className={menuOpen ? 'overflow-hidden h-screen' : ''}>
      <Head>
        <title>Tengis Movie Theater .::. Тэнгис кино теарт</title>
        <meta property="og:site_name" content="www.tengis.mn" />
        <meta property="og:title" content="Tengis Movie Theater | Тэнгис кино теарт" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://www.tengis.mn/" />
        <meta property="og:image" content="https://www.tengis.mn/static/tengis.jpg" />
        <meta
          property="og:description"
          content="“Тэнгис” кино театр нь кино үзвэрийн хамгийн сүүлийн үеийн техникээр иж бүрэн тоноглогдсон үзвэрийн 4-н танхимтай бөгөөд нэгэн зэрэг 1160-н хүнд үйлчлэх хүчин чадалтай, монгол болон дэлхийн бусад орнуудын шилдэг кинонуудыг 35 мм-ийн кино хальс ба 3D технологийг ашиглан үзэгчдэд хүргэдэг."
        />
        <meta
          name="description"
          content="“Тэнгис” кино театр нь кино үзвэрийн хамгийн сүүлийн үеийн техникээр иж бүрэн тоноглогдсон үзвэрийн 4-н танхимтай бөгөөд нэгэн зэрэг 1160-н хүнд үйлчлэх хүчин чадалтай, монгол болон дэлхийн бусад орнуудын шилдэг кинонуудыг 35 мм-ийн кино хальс ба 3D технологийг ашиглан үзэгчдэд хүргэдэг."
        />
        <FavIcon />
      </Head>
      <OffMenu menus={mainMenus} open={menuOpen} onClose={() => setMenuOpen(false)} />
      <NavigationBar menus={mainMenus} handleMenuOpen={() => setMenuOpen(true)} />
      <Carousels result={carousels} />
      <DayPicker days={days} active={day} onDayChange={setDay} />
      <MoviesSection id="movies" title="Тасалбар захиалга" result={ongoings as FullMovie[]} isOrdering={true} />
      <MoviesSectionBig id="preordering" title="Урьдчилсан захиалга" result={preorderings} />
      <MoviesSectionBig id="upcoming" title="Удахгүй дэлгэцнээ" result={upcomings} />
      <PriceSection />
      <GameSection />
      <Footer socialMenus={socialMenus} footMenus={footMenus} />
    </div>
  );
}

export const getStaticProps = async () => {
  const mainMenus = await getMenusByKey('main');
  const socialMenus = await getMenusByKey('social');
  const footMenus = await getMenusByKey('foot');
  const ongoingsMap: { [key: string]: FullMovie[] } = {};
  const days = await getDays();
  for (let day of days) {
    const movies = await getMovies({ type: '', orderType: 'desc', sessionDay: day });
    ongoingsMap[day] = movies;
  }

  const preorderings = await getMovies({ type: 'PREORDERING', orderType: 'asc' });
  const upcomings = await getMovies({ type: 'UPCOMING', orderType: 'asc' });
  const carousels = await getCarousels();

  const repo: Repo = { mainMenus, socialMenus, footMenus, ongoingsMap, preorderings, upcomings, carousels, days };

  return {
    props: { repo },
    revalidate: 60 * 5,
  };
};
