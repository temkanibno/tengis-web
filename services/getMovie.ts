import axios from 'axios';

export const getMovie = async (slug: string) => {
  const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/movies/slug/${slug}`);
  const { data } = response;
  const { result } = data;
  return result;
};
