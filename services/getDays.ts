import axios from 'axios';
import dayjs from 'dayjs';

export const getDays = async () => {
  const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/movies/sessions/days`);
  const { data } = response;
  const { result } = data;

  const days = new Set();
  for (let { day } of result) {
    if (day) {
      const dayString = new Date(day).toISOString();
      if (!days.has(dayString)) days.add(dayString);
    }
  }
  const daysArray: string[] = Array.from(days) as string[];
  const sortedDates = daysArray.sort((a, b) => new Date(a).getTime() - new Date(b).getTime());

  return sortedDates.map((date) => dayjs(date).format('YYYY-MM-DD'));
};
