import axios from 'axios';

export const getMenusByKey = async (key: string) => {
  try {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/menus/key/${key}`);
    const { data: responseData } = response;
    const { result: menus } = responseData;
    return menus;
  } catch (error) {
    return [];
  }
};
