import { StaticPage } from '@prisma/client';
import axios from 'axios';

export const getPage = async (slug: string): Promise<StaticPage> => {
  const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/staticPages/slug/${slug}`);
  const { data } = response;
  const { result } = data;
  return result;
};
