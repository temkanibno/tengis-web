import axios from 'axios';

export const getCarousels = async () => {
  const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/carousels?status=ENABLED`);
  const { data } = response;
  const { result } = data;
  return result;
};
