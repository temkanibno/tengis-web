import axios from 'axios';

type GetOngoingMoviesProps = {
  type: 'ONGOING' | 'UPCOMING' | 'PREORDERING' | '';
  sessionDay?: string;
  orderType: 'desc' | 'asc';
};

export const getMovies = async ({ type, sessionDay, orderType }: GetOngoingMoviesProps) => {
  const params = new URLSearchParams();
  if (sessionDay) params.set('sessionDay', sessionDay);
  params.set('type', type);
  params.set('orderColumn', 'nationalOpenDate');
  params.set('orderType', orderType);
  const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/movies?${params.toString()}`);
  const { data } = response;
  const { result } = data;
  return result;
};
