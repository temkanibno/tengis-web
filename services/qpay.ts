import axios from 'axios';

export const checkQpayBySenderNo = async (senderNo: string) => {
  const response = await axios.get(`/api/qpay/check?senderNo=${senderNo}`);
  const { data } = response;
  return data.response;
};
