import axios from 'axios';

export const getSession = async (id: string) => {
  const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/movies/sessions/${id}`);
  const { data } = response;
  const { result } = data;
  return result;
};
