import axios from 'axios';

export const createUserSession = async () => {
  try {
    const createSessionResponse = await axios.post(`https://www.tengis.mn/api/movies/sessions/order/create-session`, {
      // const createSessionResponse = await axios.post(`http://localhost:3000/api/movies/sessions/order/create-session`, {
      key: process.env.JWT_PUBLIC_KEY,
    });
    const { result } = createSessionResponse.data;
    return result;
  } catch (error: any) {
    console.error('======== CREATE USER SESSION FAIL==========');
    console.error(error.response.data);
  }
};
