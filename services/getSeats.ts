import axios from 'axios';

export const getSeats = async (sessionId: string) => {
  try {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/movies/sessions/seats/${sessionId}`);
    const { data } = response;
    const { result } = data;
    return result;
  } catch (error: any) {
    console.error(error.response.data);
  }
};
