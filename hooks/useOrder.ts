import { MovieSessionSeat, UserSession } from '@/types';
import { MovieSessionTicket } from '@prisma/client';
import axios from 'axios';
import { toast } from 'react-toastify';
import { create } from 'zustand';

type BookingTicket = {
  ticket: MovieSessionTicket;
  quantity: number;
};

type Order = {
  userSession: UserSession;
  tickets: BookingTicket[];
  selectedSeats: MovieSessionSeat[];
  name: string;
  email: string;
  phone: string;
};

type TicketStore = {
  loading: boolean;
  orders: { [key: string]: Order };
  setOrders: (orders: { [key: string]: Order }) => void;
  clearOrders: () => void;
  finishOrder: (userSessionId: string) => Promise<any>;
};

export const useOrder = create<TicketStore>((set) => ({
  loading: true,
  orders: {},
  setOrders: (orders: { [key: string]: Order }) => {
    set({ orders });
  },
  clearOrders: () => set({ orders: {} }),
  finishOrder: async (userSessionId: string) => {
    try {
      const response = await axios.post('/api/movies/sessions/order/confirm', { userSessionId });
      const { data } = response;
      toast.success('Захиалга амжилттай илгээгдлээ');
      return data;
    } catch (error: any) {
      const { data } = error.response;
      const { message } = data;
      toast.error(message || 'Сервер дээр алдаа гарлаа');
    }
  },
}));
