import { Order, Prisma } from '@prisma/client';

export type FullMovie = Prisma.MovieGetPayload<{ include: { genres: { include: { genre: true } }; sessions: true } }>;

export type FullSession = Prisma.MovieSessionGetPayload<{ include: { movie: true; tickets: true } }>;

export type MovieAttribute = {
  Description: string;
  Url: string;
};

export type MovieSessionSeat = {
  id?: string;
  reserved?: boolean;
  isSeat: boolean;
  areaNumber?: string;
  seatName?: string;
  areaCategoryCode?: string;
  rowIndex?: number;
  colIndex?: number;
  rowName?: string;
};

export type MovieSessionArea = {
  number: number;
  name: string;
  categoryId: string;
  seats: MovieSessionSeat[][];
};

export type UserSession = {
  accessToken: string;
  expireDate: Date;
  sessionId: string;
};

export type OrderResponse = {
  qpay: Prisma.QpayInvoiceGetPayload<{ include: { response: { include: { urls: true } } } }>;
} & Prisma.OrderGetPayload<{ include: { seats: true; tickets: true } }>;
