export const currencyFormat = (amount: number | string | bigint) => {
  const formattedAmount = new Intl.NumberFormat('mn-MN', {
    currency: 'MNT',
    maximumFractionDigits: 0,
  }).format(Number(amount));
  return `${formattedAmount} ₮`;
};
