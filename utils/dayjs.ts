import dayjs from 'dayjs';

const weekDay: { [key: string]: string } = {
  Monday: 'Дав',
  Tuesday: 'Мяг',
  Wednesday: 'Лха',
  Thursday: 'Пүр',
  Friday: 'Баа',
  Saturday: 'Бям',
  Sunday: 'Ням',
};
export const formatDate = (value: Date, format: string) => {
  return dayjs(value).format(format);
};

export const getWeekDay = (day: string) => {
  const rawWeekDay = dayjs(day).format('dddd');
  return weekDay[rawWeekDay];
};
