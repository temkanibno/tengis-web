import dayjs from 'dayjs';

export const getDayText = (showTime: Date | string) => {
  let day = dayjs(showTime).format('YYYY-MM-DD');
  const isToday = day === dayjs().format('YYYY-MM-DD');
  const isTomorrow = day === dayjs().add(1, 'day').format('YYYY-MM-DD');
  if (isToday) return 'Өнөөдөр';
  if (isTomorrow) return 'Маргааш';
  return day;
};
