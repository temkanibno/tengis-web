/** @type {import('tailwindcss').Config} */
module.exports = {
  corePlugins: {
    container: false,
  },

  content: [
    './app/**/*.{js,ts,jsx,tsx,mdx}',
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',

    // Or if using `src` directory:
    './src/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        primary: '#e51937',
        secondary: '#fff',
        purple: '#1abc9c',
        'black-light': '#242424',
        'gray-light': '#d4d4d4',
        'medium-gray': '#b4b4b4',
        'background-color': '#242424',
        'dark-gray': '#616161',
      },
    },
  },

  plugins: [
    function ({ addComponents }) {
      addComponents({
        '.container': {
          maxWidth: '100%',
          margin: '0 auto',
          padding: '0 12px',
          '@screen sm': {
            maxWidth: '540px',
          },
          '@screen md': {
            maxWidth: '960px',
          },
          '@screen lg': {
            maxWidth: '1140px',
          },
          '@screen xl': {
            maxWidth: '1320px',
          },
        },
      });
    },
  ],
};
