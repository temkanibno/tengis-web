import styles from './fail.module.css';

export const FailIcon = () => (
  <svg
    version="1.1"
    className={styles.failSvg}
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    x="0px"
    y="0px"
    width="50px"
    height="50px"
    viewBox="0 0 50 50"
    enableBackground="new 0 0 50 50"
    xmlSpace="preserve"
  >
    <g>
      <line
        className={styles.failSvg__path2}
        fill="none"
        stroke="#BB0A1E
"
        strokeWidth="3"
        strokeMiterlimit="10"
        x1="8.5"
        y1="41.5"
        x2="41.5"
        y2="8.5"
      />
      <line
        className={styles.failSvg__path3}
        fill="none"
        stroke="#BB0A1E
"
        strokeWidth="3"
        strokeMiterlimit="10"
        x1="41.5"
        y1="41.5"
        x2="8.5"
        y2="8.5"
      />
    </g>
  </svg>
);
