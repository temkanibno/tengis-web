import styles from './loader.module.css';

export const Loader = () => {
  return (
    <div className={styles.loadingWrapper}>
      <div className={styles.loadingText}>Уншиж байна</div>
      <div className={styles.loadingContent}></div>
    </div>
  );
};
