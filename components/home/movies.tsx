import { Card } from '../shared/card';
import { SectionHeader } from '../shared/section-header';
import { FullMovie } from '@/types';

interface MoviesSectionProps {
  result: FullMovie[];
  title: string;
  subtitle?: string;
  id?: string;
  isOrdering?: boolean;
}

export const MoviesSection: React.FC<MoviesSectionProps> = (props) => {
  const { result, id, title, subtitle, isOrdering } = props;
  if (!result || result.length === 0) return null;
  return (
    <div className="container" id={id}>
      <section className="py-20">
        <SectionHeader>
          {title}
          {subtitle && <span className="text-xl text-gray-light">{subtitle}</span>}
        </SectionHeader>
        <div className={`grid md:grid-cols-2 grid-cols-1 gap-6`}>
          {result.map((item, index) => (
            <Card movie={item} key={index} isOrdering={isOrdering} />
          ))}
        </div>
      </section>
    </div>
  );
};
