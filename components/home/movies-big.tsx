import { Card } from '../shared/card';
import { SectionHeader } from '../shared/section-header';
import { FullMovie } from '@/types';

interface MoviesSectionProps {
  result: FullMovie[];
  title: string;
  subtitle?: string;
  id?: string;
}

export const MoviesSectionBig: React.FC<MoviesSectionProps> = (props) => {
  const { result, id, title, subtitle } = props;
  if (result.length === 0) return null;
  return (
    <div className="container" id={id}>
      <section className="py-20">
        <SectionHeader>
          {title}
          {subtitle && <span className="text-xl text-gray-light">{subtitle}</span>}
        </SectionHeader>
        <div className={`grid md:grid-cols-3 grid-cols-1 gap-6`}>
          {result.map((item, index) => (
            <Card movie={item} key={index} />
          ))}
        </div>
      </section>
    </div>
  );
};
