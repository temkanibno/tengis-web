import Link from 'next/link';

export const GameSection: React.FC = () => {
  return (
    <section id="game">
      <Link href="/pages/togloomyn-toev">
        <div className="bg-cover	bg-center	bg-no-repeat md:h-96 h-48 flex items-center justify-center" style={{ backgroundImage: `url('/static/game.webp')` }}>
          <div className="md:text-9xl sm:text-5xl text-2xl text-center uppercase text-white drop-shadow-md" style={{ textShadow: '1px 1px 10px pink' }}>
            Тоглоомын төв
          </div>
        </div>
      </Link>
      <Link href="/pages/hool-amttan">
        <div
          className="bg-cover	bg-center	bg-no-repeat md:h-96 h-48 flex items-center justify-center  font-xxl"
          style={{
            backgroundImage: `url('/static/pop.webp')`,
          }}
        >
          <div className="md:text-9xl sm:text-5xl text-2xl uppercase text-white drop-shadow-md text-center" style={{ textShadow: '1px 1px 10px pink' }}>
            ХООЛ, АМТТАН
          </div>
        </div>
      </Link>
    </section>
  );
};
