import { getWeekDay } from '@/utils/dayjs';
import dayjs from 'dayjs';

export type DayPickerProps = {
  days: string[];
  active: string;
  onDayChange: (day: string) => void;
};
export const DayPicker: React.FC<DayPickerProps> = (props) => {
  const { days, active, onDayChange } = props;
  return (
    <div>
      <div className="flex flex-row justify-center gap-4 my-6 -mb-12 flex-wrap md:px-0 px-6">
        {days.map((day, index) => (
          <div key={`day-${index}`} className="group cursor-pointer" onClick={() => onDayChange(day)}>
            <div className="text-white text-lg mb-3 text-center">{dayjs(day).format('MM-DD')}</div>
            <div className={`text-white text-3xl uppercase group-hover:text-purple text-center ${active === dayjs(day).format('YYYY-MM-DD') && '!text-primary'}`}>{getWeekDay(day)}</div>
          </div>
        ))}
      </div>
    </div>
  );
};
