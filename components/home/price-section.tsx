import { currencyFormat } from '@/utils/currency-formatter';
import { SectionHeader } from '../shared/section-header';

export const PriceSection: React.FC = () => {
  return (
    <section id="price">
      <div className="container">
        <SectionHeader>Үнэ урамшуулал</SectionHeader>
      </div>
      <div className="bg-center bg-no-repeat bg-cover" style={{ backgroundImage: `url('https://cdn.pixabay.com/photo/2017/11/24/10/43/ticket-2974645_1280.jpg')` }}>
        <div className="container">
          <div className="py-12 overflow-x-auto">
            <table className="table-auto min-w-[600px] w-full text-center price-table text-white backdrop-filter backdrop-blur-lg bg-opacity-20 bg-white overflow-hidden rounded-lg shadow-lg p-4">
              <thead>
                <tr className="duration-300 bg-white/0 hover:bg-white/20">
                  <td className="px-4 py-2 text-left border-b">Тасалбар</td>
                  <td className="px-4 py-2 border-b">
                    Том хүн
                    <br /> (16-с дээш нас)
                  </td>
                  <td className="px-4 py-2 border-b">
                    Хүүхэд <br />
                    (4-15 нас)
                  </td>
                  <td className="px-4 py-2 text-right border-b">
                    Гишүүнчлэлийн <br />
                    оноо*
                  </td>
                </tr>
              </thead>
              <tbody>
                <tr className="duration-300 bg-white/0 hover:bg-white/20">
                  <td className="px-4 py-2 border-b" colSpan={4}>
                    Үндсэн тариф
                  </td>
                </tr>
                <tr className="duration-300 bg-white/0 hover:bg-white/20">
                  <td className="px-4 py-2 text-left border-b">2D кино</td>
                  <td className="px-4 py-2 border-b">{currencyFormat(12_000)}</td>
                  <td className="px-4 py-2 border-b">{currencyFormat(10_000)}&nbsp;</td>
                  <td className="px-4 py-2 text-right border-b">1 оноо</td>
                </tr>
                <tr className="duration-300 bg-white/0 hover:bg-white/20">
                  <td className="px-4 py-2 text-left border-b">3D кино / Онцгой үзвэр</td>
                  <td className="px-4 py-2 border-b">{currencyFormat(13_000)}</td>
                  <td className="px-4 py-2 border-b">{currencyFormat(11_000)}</td>
                  <td className="px-4 py-2 text-right border-b">1 оноо</td>
                </tr>
                <tr className="duration-300 bg-white/0 hover:bg-white/20">
                  <td className="px-4 py-2 text-left border-b">2D кино - Deluxe суудал</td>
                  <td className="px-4 py-2 border-b">{currencyFormat(15_000)}</td>
                  <td className="px-4 py-2 border-b">{currencyFormat(12_000)}</td>
                  <td className="px-4 py-2 text-right border-b">1 оноо</td>
                </tr>
                <tr className="duration-300 bg-white/0 hover:bg-white/20">
                  <td className="px-4 py-2 text-left border-b">3D кино / Онцгой үзвэр - Deluxe суудал</td>
                  <td className="px-4 py-2 border-b">{currencyFormat(16_000)}</td>
                  <td className="px-4 py-2 border-b">{currencyFormat(13_000)}</td>
                  <td className="px-4 py-2 text-right border-b">1 оноо</td>
                </tr>
                <tr className="duration-300 bg-white/0 hover:bg-white/20">
                  <td className="px-4 py-2 text-left border-b">VIP үзвэр</td>
                  <td className="px-4 py-2 border-b">{currencyFormat(20_000)}</td>
                  <td className="px-4 py-2 border-b">{currencyFormat(15_000)}</td>
                  <td className="px-4 py-2 text-right border-b">2 оноо</td>
                </tr>
                <tr className="duration-300 bg-white/0 hover:bg-white/20">
                  <td className="px-4 py-2 border-b" colSpan={4}>
                    Хямдралтай тариф
                  </td>
                </tr>
                <tr className="duration-300 bg-white/0 hover:bg-white/20">
                  <td className="px-4 py-2 text-left">Өдөр бүрийн эхний үзвэр</td>
                  <td className="px-4 py-2">{currencyFormat(10_000)}</td>
                  <td className="px-4 py-2">{currencyFormat(8_000)}</td>
                  <td className="px-4 py-2 text-right">1 оноо</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  );
};
