import { Chip } from './shared/chip';
import Slider, { Settings } from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { IconChevronLeft, IconChevronRight } from '@tabler/icons-react';
import Link from 'next/link';
import { Carousel } from '@prisma/client';
import Image from 'next/image';

const Navigator = (props: any) => {
  const { onClick, left = true } = props;
  return (
    <button
      className={`w-12 h-12 grid place-items-center rounded-full bg-black/30 group-hover:opacity-100 group-hover:visible opacity-0 invisible transition-all absolute z-20 duration-300 top-1/2	-translate-y-1/2	text-white ${
        left ? 'left-6' : 'right-6'
      }`}
      onClick={onClick}
    >
      {left ? <IconChevronLeft width={32} height={32} /> : <IconChevronRight width={32} height={32} />}
    </button>
  );
};
const sliderSettings: Settings = {
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  fade: true,
  autoplay: true,
  autoplaySpeed: 8000,
  pauseOnFocus: true,
  prevArrow: <Navigator />,
  className: 'group',
  nextArrow: <Navigator left={false} />,
};
export const Carousels = ({ result }: { result: Carousel[] }) => {
  return (
    <Slider {...sliderSettings}>
      {/* @ts-ignore */}
      {result.map((carousel: Carousel) => (
        <div key={carousel.id}>
          <div className="min-h-[700px] relative flex align-middle">
            <Image
              src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${carousel.imagePath}_large.webp`}
              width={1080}
              height={700}
              alt={carousel.title}
              loading="eager"
              className="absolute left-0 top-0 w-full h-full object-cover"
            />
            <div className="absolute left-0 top-0 w-full h-full bg-background-color/90" />
            <div className="container relative flex align-middle z-10">
              <div className="grid grid-cols-12 gap-4 items-center justify-center">
                <div className="lg:col-span-5 col-span-12 p-4">
                  <h2 className="text-7xl text-white uppercase font-bold text-left mb-5">{carousel.title}</h2>
                  <p className="text-white text-3xl uppercase mb-5">{carousel.genre}</p>
                  <div className="mb-5">
                    <Chip active>{carousel.rating}</Chip>
                  </div>
                  <p className="text-white text-3xl uppercase mb-5">{carousel.openDate}</p>
                  <p className="text-gray-light mb-5 text-justify line-clamp-4 overflow-hidden">{carousel.description}</p>
                  <Link
                    href={carousel.link || '#'}
                    className="inline-block transition-all duration-300 text-white border-2 py-2 px-8 rounded-lg text-4xl uppercase border-primary hover:text-white hover:bg-primary"
                  >
                    Дэлгэрэнгүй
                  </Link>
                </div>
                <div className="col-span-7 p-4 hidden lg:block">
                  <div className="h-[600px] relative">
                    <Image
                      src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${carousel.imagePath}_large.webp`}
                      alt={carousel.title}
                      width={750}
                      height={600}
                      loading="eager"
                      className="absolute top-0 left-0 w-full h-full object-cover rounded-3xl"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </Slider>
  );
};
