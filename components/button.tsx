import { twMerge } from 'tailwind-merge';

type ButtonVariant = 'chip' | 'button' | 'large';

interface ButtonProps {
  variant?: ButtonVariant;
  className?: string;
  as?: React.ElementType;
  [x: string]: any;
  children: React.ReactNode;
}
export const Button: React.FC<ButtonProps> = (props) => {
  const { variant = 'button', className, as, children, ...otherProps } = props;
  let Tag: React.ElementType = 'button';
  if (as) {
    Tag = as;
  }
  let defaultClasses =
    'inline-block px-2.5 py-1 border-2 border-medium-grey border-solid rounded-lg uppercase font-normal text-16 text-medium-grey text-white transition-all duration-300 bg-background-color';
  return (
    <Tag className={twMerge(defaultClasses, className)} {...otherProps} title="" aria-label="">
      {children}
    </Tag>
  );
};
