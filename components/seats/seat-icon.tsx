export type SeatIconColor = 'green' | 'yellow' | 'red' | 'gray';

export const SeatIcon = ({ color }: { color: SeatIconColor }) => {
  let fillColor = '#898989';
  switch (color) {
    case 'green':
      fillColor = '#78AC66';
      break;
    case 'red':
      fillColor = '#BB576F';
      break;
    case 'yellow':
      fillColor = '#DECD34';
      break;
  }
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" strokeWidth="1.5" stroke={fillColor} fill="none" strokeLinecap="round" strokeLinejoin="round">
      <path stroke="none" d="M0 0h24v24H0z" fill="none" />
      <path d="M5 11a2 2 0 0 1 2 2v2h10v-2a2 2 0 1 1 4 0v4a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2v-4a2 2 0 0 1 2 -2z" />
      <path d="M5 11v-5a3 3 0 0 1 3 -3h8a3 3 0 0 1 3 3v5" />
      <path d="M6 19v2" />
      <path d="M18 19v2" />
    </svg>
  );
};
