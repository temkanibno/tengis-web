import { MovieSessionSeat } from '@/types';
import { nanoid } from 'nanoid';
import { SeatIcon, SeatIconColor } from './seat-icon';
import { useEffect, useRef } from 'react';
import { VipSeatIcon } from './vip-seat-icon';
import { WheelChair } from './wheel-chair-icon';

export const SeatItems = ({
  screenName,
  seats,
  onSeatSelect,
  selectedSeats,
}: {
  screenName: string;
  seats: MovieSessionSeat[][];
  selectedSeats: MovieSessionSeat[];
  onSeatSelect: (seat: MovieSessionSeat) => void;
}) => {
  const wrapperRef = useRef<HTMLDivElement>(null);

  let hasDeluxe = false;

  useEffect(() => {
    if (wrapperRef.current) {
      wrapperRef.current.style.minWidth = `${seats[0].length * 36 + 50}px`;
    }
  }, [wrapperRef, seats]);

  const rows = seats?.map((row, rowIndex) => {
    const cols = row.map((seat) => {
      let color: SeatIconColor = 'gray';
      const isSelected = selectedSeats.find((s) => s.id === seat.id);
      const isDeluxe = seat.areaCategoryCode === '0000000001';
      const isWheelChair = screenName === 'Танхим 6' && seat.rowName === '12' && seat.seatName === '4';

      if (isDeluxe && !hasDeluxe) hasDeluxe = true;

      if (seat.reserved) {
        color = 'yellow';
      }
      if (isSelected) {
        color = 'green';
      }
      const handleOnClick = () => {
        if (seat.reserved) return;
        if (isWheelChair && !isSelected) alert('Тэргэнцэрт зориулсан талбай. Энд сандал байхгүй тул сонголтоо шалгана уу');
        onSeatSelect(seat);
      };
      return (
        <div
          className={`relative ${seat.isSeat ? 'cursor-pointer' : ''}`}
          key={nanoid()}
          onClick={() => {
            if (seat.isSeat) handleOnClick();
          }}
          data-id={seat.id}
        >
          {Boolean(seat.seatName) ? (
            <>
              {isWheelChair ? <WheelChair color={color} /> : isDeluxe ? <VipSeatIcon color={color} /> : <SeatIcon color={color} />}
              {isWheelChair ? (
                <></>
              ) : isDeluxe ? (
                <p className="absolute left-1/2 -translate-x-1/2 top-0.5 text-xs text-white">{seat.seatName}</p>
              ) : (
                <p className="absolute text-xs text-white -translate-x-1/2 left-1/2 top-1">{seat.seatName}</p>
              )}
            </>
          ) : (
            <div className="w-8 h-8" />
          )}
        </div>
      );
    });

    cols.push(
      <p key={nanoid()} className="w-6 font-bold text-center text-white">
        {rowIndex + 1}
      </p>
    );

    return (
      <div key={nanoid()} className="flex gap-1">
        {cols}
      </div>
    );
  });

  return (
    <div className="overflow-x-auto">
      <div className="flex flex-col items-center justify-center" ref={wrapperRef}>
        {rows}
        <div className="flex gap-10 my-4">
          {hasDeluxe && (
            <div className="flex items-center gap-2 text-white">
              <VipSeatIcon color="gray" /> Deluxe
            </div>
          )}

          <div className="flex items-center gap-2 text-white">
            <SeatIcon color="gray" /> Боломжтой
          </div>
          <div className="flex items-center gap-2 text-white">
            <SeatIcon color="yellow" /> Зарагдсан
          </div>
          <div className="flex items-center gap-2 text-white">
            <SeatIcon color="green" /> Сонгосон
          </div>
        </div>
      </div>
    </div>
  );
};
