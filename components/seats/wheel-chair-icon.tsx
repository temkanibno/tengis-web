export type WheelChairColor = 'green' | 'yellow' | 'red' | 'gray';

export const WheelChair = ({ color }: { color: WheelChairColor }) => {
  let fillColor = '#898989';
  switch (color) {
    case 'green':
      fillColor = '#78AC66';
      break;
    case 'red':
      fillColor = '#BB576F';
      break;
    case 'yellow':
      fillColor = '#DECD34';
      break;
  }
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" strokeWidth="1.5" stroke={fillColor} fill="none" strokeLinecap="round" strokeLinejoin="round">
      <path stroke="none" d="M0 0h24v24H0z" fill="none" />
      <path d="M11 5m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0" />
      <path d="M11 7l0 8l4 0l4 5" />
      <path d="M11 11l5 0" />
      <path d="M7 11.5a5 5 0 1 0 6 7.5" />
    </svg>
  );
};
