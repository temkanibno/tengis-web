import { Chip } from './chip';
import { Url } from 'next/dist/shared/lib/router/router';
import { FullSession } from '@/types';
import Link from 'next/link';

type ProgressProps = {
  step: 3 | 4 | 5;
  session: FullSession;
};

const ProgressChip = ({ active, children, href }: { active?: boolean; children: React.ReactNode; href: Url }) => {
  if (active)
    return (
      <Link href={href}>
        <Chip active className="rounded-full">
          {children}
        </Chip>
      </Link>
    );
  return (
    <Link href={href}>
      <Chip className="rounded-full bg-background-color">{children}</Chip>
    </Link>
  );
};

export const Progress: React.FC<ProgressProps> = (props) => {
  const { step, session } = props;
  return (
    <div className="relative my-6">
      <div className="w-full absolute h-1 rounded bg-black/20 top-[14px]">
        <div className="absolute left-0 h-full rounded bg-primary" style={{ width: `${(step - 1) * 25}%` }}></div>
      </div>
      <div className="flex justify-between">
        <div className="text-white flex flex-col items-start relative">
          <ProgressChip active href={'/#movies'}>
            1
          </ProgressChip>
          Кино сонголт
        </div>
        <div className="text-white flex flex-col items-center relative">
          <ProgressChip active href={`/film/${session.movie.slug}`}>
            2
          </ProgressChip>
          Цаг сонголт
        </div>
        <div className="text-white flex flex-col items-center relative">
          <ProgressChip href={`/film/${session.movie.slug}/session/${session.id}`} active>
            3
          </ProgressChip>
          Суудал сонголт
        </div>
        <div className="text-white flex flex-col items-center relative">
          <ProgressChip active={step >= 4} href={'javascript:void(0)'}>
            4
          </ProgressChip>
          Төлбөр төлөлт
        </div>
        <div className="text-white flex flex-col relative items-end">
          <ProgressChip active={step === 5} href={'javascript:void(0)'}>
            5
          </ProgressChip>
          Амжилттай
        </div>
      </div>
    </div>
  );
};
