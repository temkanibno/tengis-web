import { Menu } from '@prisma/client';
import Link from 'next/link';

export const OffMenu = ({ menus, open, onClose }: { menus: Menu[]; open: boolean; onClose: () => void }) => {
  if (!open) return null;
  return (
    <div className={`fixed top-0 z-50 left-0 w-full h-full flex items-center justify-center transition-all duration-300 `}>
      <div className="absolute top-0 left-0 w-full h-full bg-black/80" onClick={onClose} />
      <div className=" relative">
        <div className="p-4">
          <ul className="flex flex-col gap-4">
            {menus.map((item: Menu, index: number) => {
              return (
                <li key={index} className="text-center" onClick={onClose}>
                  <Link title={item.title} href={item.link} target={item.newTab ? '_blank' : '_self'} className="text-white text-3xl font-normal hover:text-primary transition-colors duration-300">
                    {item.title}
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </div>
  );
};
