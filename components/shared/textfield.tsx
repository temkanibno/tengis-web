import { nanoid } from 'nanoid';

type TextFieldProps = {
  label?: string;
  placeholder?: string;
  helperText?: string;
  pattern?: string;
  value?: string;
  type?: 'text' | 'email' | 'password' | 'tel';
  name?: string;
  icon?: React.ReactNode;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
};
export const TextField: React.FC<TextFieldProps> = (props) => {
  const { label, placeholder, pattern, value, type = 'text', helperText, name, icon, onChange, onBlur } = props;
  const id = nanoid();
  const helperId = nanoid();

  return (
    <div>
      {label && (
        <label htmlFor={id} className="block mb-2 text-sm font-medium text-white ">
          {label}
        </label>
      )}
      <div className="relative">
        {icon && <div className="absolute inset-y-0 left-0 flex items-center pl-2.5 pointer-events-none">{icon}</div>}
        <input
          id={id}
          aria-describedby={helperId}
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-purple focus:border-purple block w-full ps-10 p-2.5  "
          {...{ pattern, placeholder, type, value, onChange, name, onBlur }}
        />
      </div>
      {helperText && (
        <p id={helperId} className="mt-2 text-sm text-primary ">
          {helperText}
        </p>
      )}
    </div>
  );
};
