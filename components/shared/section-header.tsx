interface SectionHeaderProps {
  children: React.ReactNode;
}
export const SectionHeader: React.FC<SectionHeaderProps> = ({ children }) => {
  return <h2 className="flex w-full justify-between font-bold text-white text-4xl items-center uppercase mb-10">{children}</h2>;
};
