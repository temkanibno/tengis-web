import Image from 'next/image';
import { Chip } from './chip';
import Link from 'next/link';
import { IconBadge3d, IconVip } from '@tabler/icons-react';
import { nanoid } from 'nanoid';
import { FullMovie } from '@/types';
import { MovieSession } from '@prisma/client';
import { formatDate } from '@/utils/dayjs';
import { getDayText } from '@/utils/getDayText';

export interface CardProps {
  movie: FullMovie;
  isOrdering?: boolean;
}

export const MovieChips: React.FC<{ movie: FullMovie; isOrdering?: boolean }> = ({ movie, isOrdering }) => {
  const now = new Date().getTime();
  if (isOrdering || movie.type === 'ONGOING') {
    const vipSessions = movie.sessions.filter((session) => session.isVip);
    const normalSessions = movie.sessions.filter((session) => !session.isVip);
    return (
      <>
        {normalSessions.length > 0 && (
          <div className="flex gap-2.5 items-center flex-wrap">
            {normalSessions.map((session: MovieSession) => {
              const showTime = new Date(session.showTime).getTime();
              const doAired = showTime < now;
              if (!doAired && session.status === 'DELETED') return null;
              return (
                <Link key={session.id} href={`/film/${movie.slug}`}>
                  <Chip disabled={doAired}>
                    {formatDate(session.showTime, 'HH:mm')}
                    {session.attributes.map((attribute) => {
                      if (attribute.toLowerCase() === '3d') {
                        return <IconBadge3d key={`${session.id}-${attribute}`} />;
                      } else {
                        return null;
                      }
                    })}
                  </Chip>
                </Link>
              );
            })}
          </div>
        )}
        {vipSessions.length > 0 && (
          <div className="flex gap-2.5 items-center flex-wrap">
            {vipSessions.map((session: MovieSession) => {
              const showTime = new Date(session.showTime).getTime();
              const doAired = showTime < now;
              return (
                <Link key={session.id} href={`/film/${movie.slug}`}>
                  <Chip disabled={doAired} active className="bg-transparent">
                    {formatDate(session.showTime, 'HH:mm')}
                    {session.attributes.map((attribute) => {
                      if (attribute.toLowerCase() === '3d') {
                        return <IconBadge3d key={`${session.id}-${attribute}`} />;
                      } else {
                        return null;
                      }
                    })}
                    <IconVip />
                  </Chip>
                </Link>
              );
            })}
          </div>
        )}
      </>
    );
  }
  if (movie.type === 'PREORDERING') {
    return (
      <div className="flex gap-2.5 items-center flex-wrap">
        <Link href={`/film/${movie.slug}`}>
          <Chip>Захиалах</Chip>
        </Link>
      </div>
    );
  }
};

export const Card: React.FC<CardProps> = (props) => {
  const { movie, isOrdering } = props;
  return (
    <div className="group">
      <div className="grid sm:grid-cols-2 grid-cols-1 gap-4">
        <div>
          <div className="aspect-[300/400] relative mb-2.5 overflow-hidden">
            <Link href={`/film/${movie.slug}`}>
              <Image
                height={400}
                width={300}
                alt={movie.title}
                src={`${process.env.NEXT_PUBLIC_RESOURCE_URL}${movie.verticalPosterUrl}`}
                className="absolute w-full h-full left-0 top-0 object-cover rounded-lg group-hover:scale-105 transition-all duration-500"
              />
            </Link>
          </div>
        </div>
        <div>
          <Link href={`/film/${movie.slug}`} title={movie.title} className="text-white font-medium text-2xl group-hover:text-primary block w-full mb-4">
            {movie.title}
          </Link>
          <div className="flex flex-col gap-4">
            <div className="flex flex-wrap gap-4">
              {movie.genres.map((genre) => (
                <Link href={`/film/${movie.slug}`} key={nanoid()}>
                  <Chip active>{genre.genre.translated || genre.genre.title}</Chip>
                </Link>
              ))}
              <Link href={`/film/${movie.slug}`} key={nanoid()}>
                <Chip className="bg-transparent" active>
                  {movie.rating}
                </Chip>
              </Link>
            </div>
            {movie.type === 'ONGOING' ? (
              <div className="text-white/80 text-xl">{getDayText(movie.sessions[0]?.showTime)}</div>
            ) : (
              <div className="text-white/80 text-xl">{getDayText(movie.nationalOpenDate)}</div>
            )}
            <MovieChips movie={movie} isOrdering={isOrdering} />
          </div>
        </div>
      </div>
    </div>
  );
};
