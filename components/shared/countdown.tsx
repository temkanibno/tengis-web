import { useOrder } from '@/hooks/useOrder';
import { useRouter } from 'next/router';
import ExtCountdown from 'react-countdown';
import { toast } from 'react-toastify';

export const Countdown = ({ date }: { date: Date | null | undefined }) => {
  const router = useRouter();
  const { clearOrders } = useOrder();

  if (date === null) return null;
  return (
    <ExtCountdown
      date={date}
      renderer={({ minutes, seconds, completed }: any) => {
        if (completed) {
          toast.warn('Захиалгын хугацаа дууссан байна');
          router.push('/');
          clearOrders();
          return <></>;
        }
        return (
          <span className="bold text-secondary text-5xl">
            {minutes < 10 ? '0' : ''}
            {minutes}:{seconds < 10 ? '0' : ''}
            {seconds}
          </span>
        );
      }}
    />
  );
};
