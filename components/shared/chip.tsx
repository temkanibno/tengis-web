import { twMerge } from 'tailwind-merge';

interface ChipProps {
  children: React.ReactNode;
  active?: boolean;
  className?: string;
  disabled?: boolean;
  onClick?: () => void;
}
export const Chip: React.FC<ChipProps> = (props) => {
  const { className, children, active, onClick, disabled } = props;
  let defaultClasses =
    'transition-all duration-300 hover:bg-primary hover:border-primary hover:text-white bg-transparent border-2 border-medium-gray text-white uppercase font-normal py-0.5 px-2.5 rounded cursor-pointer inline-flex flex-nowrap gap-1 whitespace-nowrap	';
  if (active) defaultClasses = twMerge(defaultClasses, ' ', 'bg-primary hover:bg-primary text-white border-primary');
  if (disabled) defaultClasses = twMerge(defaultClasses, ' ', '!bg-medium-gray !hover:bg-medium-gray border-medium-gray hover:border-medium-gray text-white cursor-not-allowed');
  return (
    <span onClick={onClick} className={twMerge(defaultClasses, ' ', className)}>
      {children}
    </span>
  );
};
