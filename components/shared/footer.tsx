import Link from 'next/link';
import { IconBrandFacebook, IconBrandTwitter } from '@tabler/icons-react';
import { IconBrandInstagram } from '@tabler/icons-react';
import { Menu } from '@prisma/client';

export const Footer = ({ socialMenus, footMenus }: { socialMenus: Menu[]; footMenus: Menu[] }) => {
  const socialIcon = {
    facebook: <IconBrandFacebook />,
    twitter: <IconBrandTwitter />,
    instagram: <IconBrandInstagram />,
  };

  return (
    <div className="py-5">
      <div>
        <div className="container">
          <div className="flex md:justify-between md:flex-row flex-col items-center justify-center gap-2.5">
            <div>
              <ul className="flex gap-2.5">
                {socialMenus.map((item: Menu) => (
                  <li key={item.id}>
                    <Link
                      href={item.link}
                      target="_blank"
                      className="w-10 h-10 grid place-items-center rounded-full text-white border-secondary border-2 transition-all duration-300 hover:border-primary hover:bg-primary"
                    >
                      {item.title.toLowerCase() === 'facebook' ? socialIcon.facebook : item.title.toLowerCase() === 'twitter' ? socialIcon.twitter : socialIcon.instagram}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
            <div className="text-gray-light">
              <div className="flex gap-5 md:flex-row flex-col md:items-left items-center">
                {footMenus.map((item: Menu) => {
                  return (
                    <Link key={item.id} className="transition-colors duration-300 hover:text-secondary" href={item.link} target={item.newTab ? '_blank' : '_self'}>
                      {item.title}
                    </Link>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="text-gray-light text-center md:my-0 my-5 md:p-0 p-6">
        © {new Date().getFullYear()}. Бүх эрх хуулиар хамгаалагдсан.{' '}
        <Link className="transition-colors duration-300 hover:text-secondary" href={'/'}>
          Tengis
        </Link>
        .
      </div>
    </div>
  );
};
