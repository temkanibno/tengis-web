import Image from 'next/image';
import Link from 'next/link';
import { Button } from '../button';
import { IconMenu2 } from '@tabler/icons-react';
import { Menu } from '@prisma/client';

export const NavigationBar = ({ menus, handleMenuOpen }: { menus: Menu[]; handleMenuOpen: () => void }) => {
  return (
    <div className={` left-0 top-0 right-0 z-20 fixed bg-black/30 drop-shadow-2xl`}>
      <div className="container">
        <div className="p-4">
          <div className="flex justify-between align-middle">
            <Link href={'/'} className="inline-block md:w-[200px] w-[150px]">
              <Image src={'/static/logo.webp'} alt="Logo" width={1000} height={406} />
            </Link>
            <div className="flex items-center">
              <div className="mx-7 hidden lg:block">
                <ul className="flex align-middle gap-x-5">
                  {menus.map((item: Menu, index: number) => {
                    if (index > 3) return null;
                    return (
                      <li key={index}>
                        <Link
                          href={item.link}
                          target={item.newTab ? '_blank' : '_self'}
                          className={`leading-8	text-white text-xl font-normal hover:text-purple transition-colors duration-300 ${index === 0 && '!text-primary'}`}
                        >
                          {item.title}
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </div>
              <div>
                <div className="flex gap-4">
                  <Button className="border-primary hover:bg-primary hover:text-white" onClick={handleMenuOpen}>
                    <IconMenu2 />
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
