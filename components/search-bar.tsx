import { IconSearch } from "@tabler/icons-react";

export const SearchBar = () => {
  return (
    <form className="relative w-[430px] h-10  pl-11 py-1.5 overflow-hidden border-gray-light border rounded-md">
      <input type="search" className="absolute inset-0 pl-16 py-1.5 pr-5 bg-black-light focus:bg-transparent [&:focus+button]:border-white text-white" placeholder="Search" />
      <button className="absolute left-0 top-0 bottom-0 h-10 w-11 grid place-items-center border-r border-transparent bg-black-light focus text-white">
        <IconSearch />
      </button>
    </form>
  );
};
